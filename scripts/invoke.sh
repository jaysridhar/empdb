#!/bin/bash

mvn -q exec:exec -Dexec.executable=java -Dexec.args="-classpath %classpath com.empinfo.empdb.App $*"
