package com.empinfo.empdb;

import java.io.File;

import java.util.List;
import java.util.Arrays;
import java.util.Properties;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.regions.RegionUtils;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

import com.amazonaws.auth.BasicAWSCredentials;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.RangeKeyCondition;

import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.DeleteTableResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.empinfo.core.Options;

public class App
{
    static private Properties props = new Properties();
    static {
	try {
	    props.load(App.class.getResourceAsStream("/empdb.properties"));
	} catch(java.io.IOException ex) {
	    System.err.println("Unable to load properties: " +
			       ex.getMessage());
	}
    }

    static private final String VERIFICATION_NM = "verification_table";
    static final private String ACCESS_NM = "aws_access_key_id";
    static final private String SECRET_NM = "aws_secret_access_key";

    static private String tableName()
    {
	return props.getProperty(VERIFICATION_NM);
    }

    static private String accessKey()
    {
	return props.getProperty(ACCESS_NM);
    }

    static private String secretKey()
    {
	return props.getProperty(SECRET_NM);
    }

    static private void print(String mesg)
    {
	System.out.println(mesg);
    }

    static private final String enumRegions()
    {
	List<Region> regions = RegionUtils.getRegionsForService(AmazonDynamoDB.ENDPOINT_PREFIX);
	StringBuilder sbuf = new StringBuilder();
	for (Region r : regions) {
	    sbuf.append(r.getName()).append(", ");
	}
	return sbuf.toString();
    }

    static private DynamoDB createClient(String endpoint)
    {
	AmazonDynamoDBClient client = new
	    AmazonDynamoDBClient(new BasicAWSCredentials(accessKey(), secretKey())).withEndpoint(endpoint);
	return new DynamoDB(client);
    }

    static private void showTable(DynamoDB db,List<String> args)
	throws Exception
    {
	try {
	    Table table = db.getTable(tableName());
	    TableDescription d = table.describe();
	    print("Table \042" + d.getTableName() + "\042\n" +
		  "  Status: " + d.getTableStatus() + "\n" +
		  "  #Items: " + d.getItemCount() + "\n" +
		  "  Size: " + d.getTableSizeBytes() + "\n" +
		  "  Created: " + d.getCreationDateTime() + "\n" +
		  "  Table ARN: " + d.getTableArn() + "\n" +
		  "  Stream ARN: " + d.getLatestStreamArn() + "\n");
	} catch(ResourceNotFoundException ex) {
	    print(ex.getMessage());
	}
    }

    static private void dropTable(DynamoDB db,List<String> args)
	throws Exception
    {
	Table table = db.getTable(tableName());
	DeleteTableResult res = table.delete();
	print("Delete Table \042" + table + "\042\n" + res.toString());
    }

    static private void createTable(DynamoDB db,List<String> args)
	throws Exception
    {
	print("Attempting to create table; please wait...");
	Table table = db
	    .createTable(tableName(),
			 Arrays.asList(new KeySchemaElement("id",KeyType.HASH)),
			 Arrays.asList(new AttributeDefinition("id",
							       ScalarAttributeType.S)),
			 new ProvisionedThroughput(2L, 2L));
	table.waitForActive();
	print("Success.  Table status: " +
	      table.getDescription().getTableStatus());
    }

    static private void insertData(DynamoDB db,List<String> args)
	throws Exception
    {
	if ( args.size() != 1 ) {
	    print("usage: insert file.json");
	    return;
	}

        Table table = db.getTable(tableName());
        JsonNode curNode = new ObjectMapper().readTree(new File(args.get(0)));
	String id = curNode.path("id").asText();
	try {
	    table.putItem(Item.fromJSON(curNode.toString()));
	} catch (Exception e) {
	    print("Unable to add row: " + id);
	    print(e.getMessage());
	}
    }

    static public void main(String[] args) throws Exception
    {
        Region region = Region.getRegion(Regions.DEFAULT_REGION);
	Options opts = new Options();
	opts
	    .addOption("h")
            .addOption("R:",
                       "Set Region (default: " + region.getName() + ")\n" +
                       "  Choose from: " + enumRegions() + "\n")
	    .addSynopsis("[options] command {args}")
	    .addDescription
("Available commands are:\n" +
 " insert file.json\n" +
 "   Insert data from file.json into table.\n" +
 " show\n" +
 "   Show details about table.\n");

	if ( ! opts.parse(args) ) System.exit(1);

	while ( opts.hasNext() ) {
	    Options.Next next = opts.next();
	    switch(next.getOption()) {
            case 'R':
		{
		    Regions r = Regions.fromName(next.getOptionValue());
		    region = Region.getRegion(r);
		}
                break;

            case 'h':
                opts.manpageUsage(null);
                System.exit(1);
                break;

            default:
                opts.manpageUsage("Unknown option: " + next.getOption());
                System.exit(1);
	    }
	}

	if ( opts.index() == args.length ) {
	    opts.simpleUsage("JSON file needed.");
	    System.exit(1);
	}

	int index = opts.index();
	String endpoint = "https://" +
	    region.getServiceEndpoint(AmazonDynamoDB.ENDPOINT_PREFIX);
	System.out.println("Endpoint: " + endpoint);
	DynamoDB db = createClient(endpoint);
	String cmd = args[index]; index++;
	List<String> rem = Arrays.asList(args).subList(index, args.length);
	if ( cmd.equalsIgnoreCase("show") ) showTable(db, rem);
	else if ( cmd.equalsIgnoreCase("create") ) createTable(db, rem);
	else if ( cmd.equalsIgnoreCase("insert") ) insertData(db, rem);
	else if ( cmd.equalsIgnoreCase("drop") ) dropTable(db, rem);
	else throw new Exception("Unsupported command: " + cmd);
    }
}
