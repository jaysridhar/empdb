package com.empinfo.core;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.text.MessageFormat;

public class M
{
    private static final String BUNDLE_NAME =
        "com.empinfo.core.messages";

    private static final ResourceBundle RESOURCE_BUNDLE =
        ResourceBundle.getBundle( BUNDLE_NAME );

    private M() {}

    static public String fmt(String key,Object... arguments)
    {
        try {
            String fmtString = RESOURCE_BUNDLE.getString(key);
            return MessageFormat.format( fmtString, arguments );
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    static public String get(String key)
    {
        try {
            return RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
