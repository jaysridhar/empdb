package com.empinfo.core;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class Options implements Iterable<Options.Next>
{
    private String m_callerClass = null;
    private LinkedHashMap<Character,Option> m_optionsMap
	= new LinkedHashMap<Character,Option>();
    private ArrayList<String> m_synopses = new ArrayList<String>();
    private ArrayList<String> m_description = new ArrayList<String>();
    private ArrayList<Next> m_options = new ArrayList<Next>();
    private Iterator<Next> m_iterator = null;
    private int m_index = -1;

    static public class Next
    {
	private char optchar;
	private String optarg;

	public Next(char optchar)
	{
	    this.optchar = optchar;
	}

	public Next(char optchar,String optarg)
	{
	    this.optchar = optchar;
	    this.optarg = optarg;
	}

	public char getOption()
	{
	    return optchar;
	}

	public String getOptionValue()
	{
	    return optarg;
	}
    }

    static public class Option
    {
	private char	m_option = '\0';
	private char	m_valueSpec = '\0';
	private String	m_description = null;

	public Option(char option,char valueSpec,String description)
	    throws IllegalArgumentException
	{
	    this.m_option = option;
	    switch(valueSpec) {
	    case ':':
		this.m_valueSpec = valueSpec;
		break;

	    default:
		throw new IllegalArgumentException("Unsupported value specification: '" + valueSpec + "'");
	    }
	    this.m_description = description;
	}

	public Option(char option,String description)
	{
	    this.m_option = option;
	    this.m_description = description;
	}

	final public boolean isValueRequired()
	{
	    return m_valueSpec == ':';
	}

	final public char getOption()
	{
	    return m_option;
	}

	final public char getValueSpec()
	{
	    return m_valueSpec;
	}

	final public String getDescription()
	{
	    return m_description;
	}
    }

    static public class UnsupportedOptionException
	extends IllegalArgumentException
    {
	private static final long serialVersionUID = 100L;

	public UnsupportedOptionException(char c)
	    {
		super("Unsupported option: -" + c);
	    }
    }

    static public class InsufficientArgsException
	extends IllegalArgumentException
    {
	private static final long serialVersionUID = 200L;

	public InsufficientArgsException(char c)
	    {
		super("Argument required for option: -" + c);
	    }
    }

    private void synopsis()
    {
	if ( m_synopses.isEmpty() ) {
	    System.out.print("java " + m_callerClass);
	    Set<Character> keys = m_optionsMap.keySet();
	    for (Character c : keys) {
		Option option = m_optionsMap.get(c);
		String descr = option.getDescription();
		if ( descr == null || descr.isEmpty() ) continue;
		System.out.printf(" [-%c%s]",
				  c, 
				  option.isValueRequired() ? " Value" : "");
	    }
	} else {
	    for (String str : m_synopses) {
		System.out.println("java " + m_callerClass + " " + str);
	    }
	}
    }

    public Options()
    {
	Throwable t = new Throwable();
	StackTraceElement e = t.getStackTrace()[1];
	m_callerClass = e.getClassName();
    }

    /**
     * Add option to be processed, along with a description of the
     * option. The `'option' argument is specified in the following
     * forms: "c" (for an option that does not require an argument) or
     * "c:" for an option that requires an argument.
     *
     * Throws an exception if the `option' is not in one of the above
     * forms.
     */
    final public Options addOption(String option,String description)
	throws IllegalArgumentException
    {
	switch(option.length()) {
	case 1:
	    m_optionsMap.put(option.charAt(0),
			     new Option(option.charAt(0),
					description));
	    break;

	case 2:
	    m_optionsMap.put(option.charAt(0),
			     new Option(option.charAt(0),
					option.charAt(1),
					description));
	    break;

	default:
	    throw new IllegalArgumentException("Invalid option specification: "
					       + option);
	}
	return this;
    }

    /**
     * Add synopsis of the usage. Argument consists of the expected
     * form of the command line without the first part which should be
     * the app name. For multiple usage scenarios, invoke this method
     * more than once.
     */
    final public Options addSynopsis(String str)
    {
	m_synopses.add(str);
	return this;
    }


    /**
     * Add an undocumented option. The option is triggered as
     * specified by `option' which can require an argument as
     * explained above.
     */
    final public Options addOption(String option)
    {
	return addOption(option, null);
    }

    final public Options addDescription(String description)
    {
	m_description.add(description);
	return this;
    }

    /**
     * Parse the specified list of arguments. The of the arguments are
     * processed and removed from the command line. After processing,
     * the command line minus the processed options can be retrieved
     * using `args()'.
     *
     * In case of invalid options or user errors processing the
     * options, the method `usage()' is invoked. Default
     * implementation of `usage()' prints the usage and exits. It can
     * be overridden to perform further processing.
     *
     * Returns true if the parsing succeeded without errors (including
     * no command line arguments to handle). Returns false in case of
     * errors including specifying command line arguments not matching
     * the specs.
     */
    final public boolean parse(String[] args)
    {
	m_index = 0;
	for (int iz = args.length ; m_index < iz ; m_index++) {
	    String arg = args[m_index];
	    char c;
	    if ( arg.length() != 2 ) return true;
	    if ( arg.charAt(0) != '-' ) return true;
	    if ( (c = arg.charAt(1)) == '-' ) { m_index++; return true; }
	    if ( ! m_optionsMap.containsKey(c) )
		throw new UnsupportedOptionException(c);
	    Option option = m_optionsMap.get(c);
	    if ( option.isValueRequired() ) {
		m_index++;
		if ( m_index == iz )
		    throw new InsufficientArgsException(c);
		m_options.add(new Next(c, args[m_index]));
	    } else m_options.add(new Next(c));
	}
	return true;
    }

    final public int index()
    {
	return m_index;
    }

    final public Iterator<Next> iterator()
    {
	return m_options.iterator();
    }

    final public boolean hasNext()
    {
	if ( m_iterator == null ) m_iterator = iterator();
	return m_iterator.hasNext();
    }

    final public Next next()
    {
	if ( m_iterator == null ) m_iterator = iterator();
	return m_iterator.next();
    }

    final public void simpleUsage(String message)
    {
	if ( message != null ) System.out.println("*** " + message);
	synopsis();
	Set<Character> keys = m_optionsMap.keySet();
	if ( ! keys.isEmpty() ) System.out.println("Options are:");
	for (Character c : keys) {
	    Option option = m_optionsMap.get(c);
	    String descr = option.getDescription();
	    if ( descr == null || descr.isEmpty() ) continue;
	    System.out.printf("  -%c%s: %s%s\n",
			      option.getOption(),
			      option.isValueRequired() ? " Value": "",
			      option.isValueRequired() ? "": "      ",
			      option.getDescription());
	}
    }

    final public void manpageUsage(String message)
    {
	if ( message != null ) System.out.println("*** " + message);
	System.out
	    .print("\n" +
		   "SYNOPSIS\n" +
		   "\n");
	synopsis();
	if ( ! m_description.isEmpty() ) {
	    System.out
		.print("\n" +
		       "DESCRIPTION\n" +
		       "\n");
	    for (String str : m_description) {
		System.out.println("  " + str);
	    }
	    System.out.println();
	}
	Set<Character> keys = m_optionsMap.keySet();
	if ( ! keys.isEmpty() ) System.out.print("\n" +
						 "OPTIONS\n" +
						 "\n");
	for (Character c : keys) {
	    Option option = m_optionsMap.get(c);
	    String descr = option.getDescription();
	    if ( descr == null || descr.isEmpty() ) continue;
	    System.out.printf("  -%c%s\n   %s\n\n",
			      option.getOption(),
			      option.isValueRequired() ? " Value": "",
			      option.getDescription());
	}
    }
}
