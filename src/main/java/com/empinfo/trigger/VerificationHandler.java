package com.empinfo.trigger;

import java.nio.ByteBuffer;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.regions.RegionUtils;

import com.amazonaws.services.lambda.runtime.Context; 
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.RangeKeyCondition;

import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.DeleteTableResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.Record;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.log4j.Logger;

import com.empinfo.core.M;

public class VerificationHandler
    implements RequestHandler<DynamodbEvent,String>
{
    static private final Logger log = Logger.getLogger("com.empinfo.trigger");

    static private Properties props = new Properties();
    static {
	try {
	    props.load(VerificationHandler.class
		       .getResourceAsStream("/empdb.properties"));
	} catch(java.io.IOException ex) {
	    log.error("Unable to load properties from empdb.properties: " +
		      ex.getMessage());
	}
    }

    static final private String EMPDB_NM = "empdb_table";
    static final private String ACCESS_NM = "aws_access_key_id";
    static final private String SECRET_NM = "aws_secret_access_key";

    /* Endpoint is set for each request and comes from the request. */
    private String endPoint = null;

    /* Connection to DynamoDB. Depends on endPoint. If endPoint
     * changes, this connection is reset. This is because the AWS
     * region is contained in the each record of the DynamoDB
     * event. So potentially it could change between records. */
    private DynamoDB _db = null;

    /* Used for JSON conversion. */
    private ObjectMapper mapper = new ObjectMapper();

    static private String empdbTable()
    {
	return props.getProperty(EMPDB_NM);
    }

    static private String accessKey()
    {
	return props.getProperty(ACCESS_NM);
    }

    static private String secretKey()
    {
	return props.getProperty(SECRET_NM);
    }

    /* Method creates a Dynamodb connection on demand. */
    private DynamoDB db()
    {
	if ( _db != null ) return _db;
	AWSCredentials creds = new BasicAWSCredentials(accessKey(),secretKey());
	AmazonDynamoDBClient client = new
	    AmazonDynamoDBClient(creds).withEndpoint(endPoint);
	_db = new DynamoDB(client);
	log.debug("Connected to DynamoDB at: " + endPoint + ": " + _db);
	return _db;
    }

    private void setEndpoint(String regionName)
    {
	Region region = RegionUtils.getRegion(regionName);
	String str = "https://" +
	    region.getServiceEndpoint(AmazonDynamoDB.ENDPOINT_PREFIX);
	/* if endPoint changed, reset _db. */
	if ( endPoint == null || ! str.equals(endPoint) ) {
	    log.info("Reset endPoint from: " + endPoint + " to " + str);
	    _db = null;
	    endPoint = str;
	}
    }

    private Table findTable(String tableName)
    {
	try {
	    return db().getTable(tableName);
	} catch(ResourceNotFoundException ex) {
	    log.info(tableName + " does not exist.");
	    return null;
	}
    }

    /* 
     * Unpacks AttributeValue objects in normal Java objects as follows:
     *
     * Type "s" => String
     * Type "n" => Float
     * Type "b" => ByteBuffer.
     * Type "m" => Map<String,Object>
     * Type "l" => List<Object>
     * Type "ss" => List<String>
     * Type "ns" => List<Float>
     * Type "bs" => List<ByteBuffer>
     * Type "bool" => Boolean
     * Type "null" => null
     */
    private Object unpack(AttributeValue av)
    {
	if ( av.getS() != null ) return av.getS();
	else if ( av.getN() != null ) return Float.parseFloat(av.getN());
	else if ( av.getB() != null ) return av.getB().duplicate();
	else if ( av.getM() != null ) {
	    Map<String,Object> out = new HashMap<>();
	    for (String key : av.getM().keySet())
		out.put(key, unpack(av.getM().get(key)));
	    return out;
	}
	else if ( av.getL() != null ) {
	    List<Object> out = new ArrayList<>();
	    for (AttributeValue child : av.getL()) out.add(unpack(child));
	    return out;
	}
	else if ( av.getSS() != null ) {
	    List<String> out = new ArrayList<>();
	    for (String value : av.getSS()) out.add(value);
	    return out;
	}
	else if ( av.getNS() != null ) {
	    List<Float> out = new ArrayList<>();
	    for (String value : av.getNS()) out.add(Float.parseFloat(value));
	    return out;
	}
	else if ( av.getBS() != null ) {
	    List<ByteBuffer> out = new ArrayList<>();
	    for (ByteBuffer buf : av.getBS()) out.add(buf.duplicate());
	    return out;
	}
	else if ( av.isBOOL() ) return av.isBOOL();
	else return null;
    }

    private Map<String,Object> normalize(Map<String,AttributeValue> map)
    {
	Map<String,Object> out = new HashMap<>();
	for (String key : map.keySet()) out.put(key, unpack(map.get(key)));
	return out;
    }

    public String logRequest(DynamodbEvent event,Context context)
    {
	for (DynamodbStreamRecord record : event.getRecords()) {
	    try {
		String v = mapper
		    .writerWithDefaultPrettyPrinter()
		    .writeValueAsString(record);
		StringBuilder sbuf = new StringBuilder();
		sbuf.append("Event ID: ")
		    .append(record.getEventID()).append('\n')
		    .append("Event Nm: ")
		    .append(record.getEventName()).append('\n')
		    .append("Record: ").append(v);
		log.info(sbuf.toString());
		String name = record.getEventName();
		if ( name.equalsIgnoreCase("INSERT") )
		    log.info("INSERT: " + mapper
			     .writerWithDefaultPrettyPrinter()
			     .writeValueAsString(normalize(record.getDynamodb().getNewImage())));
		else if ( name.equalsIgnoreCase("MODIFY") ) {
		    log.info("MODIFY from: " + mapper
			     .writerWithDefaultPrettyPrinter()
			     .writeValueAsString(normalize(record.getDynamodb().getOldImage())));
		    log.info("To: " + mapper
			     .writerWithDefaultPrettyPrinter()
			     .writeValueAsString(normalize(record.getDynamodb().getNewImage())));
		} else if ( name.equalsIgnoreCase("REMOVE") )
		    log.info("REMOVE: " + mapper
			     .writerWithDefaultPrettyPrinter()
			     .writeValueAsString(normalize(record.getDynamodb().getOldImage())));
	    } catch(java.io.IOException ex) {
		throw new RuntimeException("Error processing record", ex);
	    }
	}

	return "Successfully processed " + event.getRecords().size() + " records.";
    }

    private void handleInsert(DynamodbEvent event,
			      Context context,
			      DynamodbStreamRecord record)
    {
	Table empdb = findTable(empdbTable());
	if ( empdb == null )
	    throw new IllegalStateException(M.fmt("tableNotFoundCreateIt",
						  empdbTable()));

	Map<String,Object> data = normalize(record.getDynamodb().getNewImage());

	/* Insertion of a NEW verification request.  Check if employer
	 * exists in the EmpDB table.
	 */
	String name = data.get("employerName").toString();
	String placeId = data.get("companyPlaceId").toString();
	GetItemSpec gis = new GetItemSpec()
	    .withPrimaryKey("employerName", name, "companyPlaceId", placeId);
	Item item = empdb.getItem(gis);
	log.info("Search for item: " + name + ", " + placeId + ": " + item);
	if ( item == null ) {
	    /* (employerName, companyPlaceId) not found in
	     * empdbTable. Insert row. */
	    log.info("inserting item into " + empdbTable());
	    item = new Item().withPrimaryKey("employerName", name,
					     "companyPlaceId", placeId);
	    empdb.putItem(item);
	    log.info("insert successful: " + name + ", " + placeId);
	} else {
	    /* Nothing to be done. Updates are handled in
	     * handleModify(). */

	    /* TODO: Do we need to update verification-table row here? To
	     * update email address, phone and any other contact
	     * information. */
	    log.info("empdb record found for: " + name + ", " + placeId);
	}
    }

    private void handleModify(DynamodbEvent event,
			      Context context,
			      DynamodbStreamRecord record)
    {
    }

    public String handleRequest(DynamodbEvent event,Context context)
    {
	for (DynamodbStreamRecord record : event.getRecords()) {
	    try {
		setEndpoint(record.getAwsRegion());
		String name = record.getEventName();
		if ( name.equalsIgnoreCase("INSERT") ) {
		    handleInsert(event, context, record);
		}
		else if ( name.equalsIgnoreCase("MODIFY") ) {
		    handleModify(event, context, record);
		}
		else if ( name.equalsIgnoreCase("REMOVE") ) {
		    /* No action on REMOVE. We keep the data that is there. */
		}
	    } catch(Exception ex) {
	    }
	}
	return "OK";
    }
}
